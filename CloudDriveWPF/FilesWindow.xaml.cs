﻿using System.Windows;
using System.Linq;
using Microsoft.Win32;
using System;
using CloudDrive.Services;

namespace CloudDriveWPF
{
    /// <summary>
    /// Логика взаимодействия для FilesWindow.xaml
    /// </summary>
    public partial class FilesWindow : Window
    {
        private DropBoxService _dropBoxService;

        public FilesWindow()
        {
            InitializeComponent();

            _dropBoxService = new DropBoxService();

            SetFilesToListBoxAsync();
        }

        private async void SetFilesToListBoxAsync()
        {
            var dropBoxEntries = await _dropBoxService.GetEntriesAsync();

            foreach (var file in dropBoxEntries.Where(entry => entry.IsFile))
            {
                dropBoxFilesList.Items.Add(file.Name);
            }
        }

        private async void LoadFileButtonClick(object sender, RoutedEventArgs e)
        {
            var openFileDialog = new OpenFileDialog();
            openFileDialog.ShowDialog();

            try
            {
                await _dropBoxService.UploadFileAsync(openFileDialog.SafeFileName, openFileDialog.FileName);
            }
            catch (ArgumentNullException)
            {
                MessageBox.Show("File wasn't selected!");
                return;
            }
            catch
            {
                MessageBox.Show("File can't be loaded!");
            }

            dropBoxFilesList.Items.Clear();
            SetFilesToListBoxAsync();
        }
    }
}
