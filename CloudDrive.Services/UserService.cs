﻿using CloudDrive.DataAccess;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace CloudDrive.Services
{
    public class UserService
    {
        public bool IsDataEmpty(string email, string password)
        {
            return (string.IsNullOrEmpty(email) || string.IsNullOrEmpty(password));
        }

        public async Task<bool> UserExistsAsync(string email)
        {
            using (var context = new DataContext())
            {
                return await context.Users.SingleOrDefaultAsync(u => u.Email == email) != null;
            }
        }

        public async Task<bool> SignInAsync (string email, string password)
        {
            using (var context = new DataContext())
            {
                var user = await context.Users.SingleOrDefaultAsync(u => u.Email == email);

                if (user == null || !SecurityHasher.VerifyPassword(password, user.Password))
                {
                    return await Task.FromResult(false);
                }
                return await Task.FromResult(true);
            }
        }

        public async Task<bool> SignUpAsync (string email, string password)
        {
            using (var context = new DataContext())
            {
                var users = context.Users.ToList();

                context.Users.Add(new User
                {
                    Email = email,
                    Password = SecurityHasher.HashPassword(password)
                });

                var result = await context.SaveChangesAsync();
                if (result != 1)
                {
                    return await Task.FromResult(false);
                }
                return await Task.FromResult(true);
            }
        }
    }
}
