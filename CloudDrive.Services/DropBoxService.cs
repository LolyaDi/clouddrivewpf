﻿using Dropbox.Api;
using Dropbox.Api.Files;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace CloudDrive.Services
{
    public class DropBoxService
    {
        private readonly string accessToken = "3_3X5F3Lr8AAAAAAAAAAEN24JY0IRK9m-9R7VQR-4LWa3Fa3l3T0jG_3knJKEp-u";

        public async Task<IList<Metadata>> GetEntriesAsync()
        {
            using (var dropBox = new DropboxClient(accessToken))
            {
                var dropBoxFiles = await dropBox.Files.ListFolderAsync(string.Empty);
                return await Task.FromResult(dropBoxFiles.Entries);
            }
        }

        public async Task UploadFileAsync(string file, string path)
        {
            using (var dropBox = new DropboxClient(accessToken))
            {
                using (var stream = new MemoryStream(File.ReadAllBytes(path)))
                {
                    await dropBox.Files.UploadAsync(
                        "/" + file,
                        WriteMode.Overwrite.Instance,
                        body: stream);
                }
            }
        }

    }
}
